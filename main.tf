provider "aws" {
  region  = "eu-west-1"
  profile = "opentrends"
}

resource "aws_s3_bucket" "bucket_main" {
  bucket = "harrinsonmb-bucket"
}

resource "aws_s3_bucket_acl" "bucket_acl" {
 depends_on = [aws_s3_bucket_ownership_controls.bucket_controls]

  bucket = aws_s3_bucket.bucket_main.id

  acl = "private"
}

resource "aws_s3_bucket_public_access_block" "bucket_acls" {
  bucket = aws_s3_bucket.bucket_main.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_ownership_controls" "bucket_controls" {
  bucket = aws_s3_bucket.bucket_main.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

data "aws_iam_policy_document" "bucket_document" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.bucket_main.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.cf-oia.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.bucket_main.id
  policy = data.aws_iam_policy_document.bucket_document.json

  depends_on = [
    aws_s3_bucket_ownership_controls.bucket_controls,
    aws_s3_bucket_public_access_block.bucket_acls # We must allow some acl features for being allowed to create this policy
  ]
}

resource "aws_cloudfront_distribution" "cf_distribution" {
  comment              = "My awesome CloudFront"
  enabled              = true
  default_root_object  = "index.html"

  origin {
    domain_name = aws_s3_bucket.bucket_main.bucket_domain_name
    origin_id   = "s3_origin"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cf-oia.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    target_origin_id = "s3_origin"

    cache_policy_id          = data.aws_cloudfront_cache_policy.cf-cache-policy.id

    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]

    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

data "aws_cloudfront_cache_policy" "cf-cache-policy" {
  id = "658327ea-f89d-4fab-a63d-7e88639e58f6"
}

resource "aws_cloudfront_origin_access_identity" "cf-oia" {
  comment = "Comentario"
}